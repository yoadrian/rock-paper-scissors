# Rock Paper Scissors

The Odin Project's Rock Paper Scissors JavaScript project, with a twist (original assignment required the game to be played in the console).  
You can play it in the browser, through prompts and alerts.  
Access the game's GitLab page at <a href="https://yoadrian.gitlab.io/rock-paper-scissors" target="_blank">https://yoadrian.gitlab.io/rock-paper-scissors</a>.  

## How to Play

You will be prompted to play a five round game of Rock-Paper-Scissors when you open the page.  
If no prompt appears, please refresh the page.  
Enter one of the words: rock, paper, scissors and press Enter.  
When the game alerts the winner of the round, press Enter to continue.   
At the end, the game alerts the overal score. Press enter to finish the game.  