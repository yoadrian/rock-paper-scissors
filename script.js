// User inputs text: rock, paper, scissors. Can be capital letters;
// Function transforms string to small caps;
function promptUser() {
    let userInput = prompt("Make your choice! Type rock, paper or scissors bellow:", '').toLowerCase();
    return userInput;
}

// Computer randomly selects rock, paper, scissors;
// Math.random() produces a floating poin nr between 0 (inclusively) and 1 (exclusively);
// * words.length() produces yet another floating point nr between 0 and 3;
// Math.floor takes the previous floating point nr and rounds it to the nearest integer;
function playComputer() {
    let words = ["rock", "paper", "scissors"];
    console.log(words.length);
    let randWord = words[Math.floor(Math.random() * words.length)];
    return randWord;
}

// playRound plays one round of RPS, keeps score and alerts the winner of the round;
// alert for score at each round disabled. Round alert considered enough + final game score;
function playRound(playerSelection, computerSelection) {
    let playerScoreRound = 0;
    let computerScoreRound = 0;
    if ((playerSelection === 'rock') && (computerSelection === 'paper')) {
        alert(`You chose '${playerSelection}' and Computer chose '${computerSelection}' \r\n You lose! Paper beats Rock.`);
        computerScoreRound++;
    } else if ((playerSelection === 'rock') && (computerSelection === 'scissors')) {
        alert(`You chose '${playerSelection}' and Computer chose '${computerSelection}' \r\n You win! Rock beats Scissors.`);
        playerScoreRound++;
    } else if ((playerSelection === 'paper') && (computerSelection === 'rock')) {
        alert(`You chose '${playerSelection}' and Computer chose '${computerSelection}' \r\n You win! Paper beats Rock.`);
        playerScoreRound++;
    } else if ((playerSelection === 'paper') && (computerSelection === 'scissors')) {
        alert(`You chose '${playerSelection}' and Computer chose '${computerSelection}' \r\n You lose! Scissors beats Paper.`);
        computerScoreRound++;
    } else if ((playerSelection === 'scissors') && (computerSelection === 'rock')) {
        alert(`You chose '${playerSelection}' and Computer chose '${computerSelection}' \r\n You lose! Rock beats Scissors.`);
        computerScoreRound++;
    } else if ((playerSelection === 'scissors') && (computerSelection === 'paper')) {
        alert(`You chose '${playerSelection}' and Computer chose '${computerSelection}' \r\n You win! Scissors beats Paper.`);
        playerScoreRound++;
    } else if (((playerSelection === 'rock') && (computerSelection === 'rock')) || ((playerSelection === 'paper') && (computerSelection === 'paper')) || ((playerSelection === 'scissors') && (computerSelection === 'scissors'))) {
        alert(`You chose '${playerSelection}' and Computer chose '${computerSelection}' \r\n It's a tie!`);
    } else {
        return alert(`You wrote '${playerSelection}' and I don't know what that is?!? :( \r\n Press 'Enter' and refresh the page (F5) to start a new game.`);
        //window.location.reload(); // This transforms the game in Rock Paper Scissors hell, you cannot get out if the input is wrong
    }
    //alert(`The score is Player ${playerScoreRound} - Computer ${computerScoreRound}`); //works, but no need to have intermediary, round score
    return [playerScoreRound, computerScoreRound]; // Returns an array that will be used in the playGame() function to keep score
        
}

// Left the variable declarations here to acknowledge how the function worked independently. Placed them in playGame() function;
//let playerSelection = promptUser();
//let computerSelection = playComputer();

//playRound(playerSelection, computerSelection);

// playGame() function that plays 5 rounds and keeps score + alerts final score;
// let score takes the array that playRound() returns at the end
// playerScoreGame adds itself to the first element of the score array
// computerScoreGame adds itself to the second element of the score array
function playGame() {
    let playerScoreGame = 0;
    let computerScoreGame = 0;
    for (let i = 0; i < 5; i++) {
        let playerSelection = promptUser();
        let computerSelection = playComputer();
        let score = playRound(playerSelection, computerSelection); 
        playerScoreGame += score[0]; 
        computerScoreGame += score[1]; 
    }
    alert(`The game score is Player ${playerScoreGame} - Computer ${computerScoreGame}`);
}

playGame();
